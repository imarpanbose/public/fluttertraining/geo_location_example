import 'package:flutter/material.dart';
import 'package:geo_location_test/location_page.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: LocationPage()
      ),
    );
  }
}
